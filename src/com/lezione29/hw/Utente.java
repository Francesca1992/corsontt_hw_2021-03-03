package com.lezione29.hw;

public class Utente {
	
	private String utenteID;
	private String nomeUtente;
	private String passwordUtente;
	private String tipoUtente;
	
	Utente() {
		
	}

	public String getUtenteID() {
		return utenteID;
	}

	public void setUtenteID(String utenteID) {
		this.utenteID = utenteID;
	}

	public String getNomeUtente() {
		return nomeUtente;
	}

	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}

	public String getPasswordUtente() {
		return passwordUtente;
	}

	public void setPasswordUtente(String passwordUtente) {
		this.passwordUtente = passwordUtente;
	}

	public String getTipoUtente() {
		return tipoUtente;
	}

	public void setTipoUtente(String tipoUtente) {
		this.tipoUtente = tipoUtente;
	}
	
	public String stampaUtente() {
		return "Utente [utenteID=" + utenteID + ", nomeUtente=" + nomeUtente + ", passwordUtente=" + passwordUtente
				+ ", tipoUtente=" + tipoUtente + "]";
	}
	
}
