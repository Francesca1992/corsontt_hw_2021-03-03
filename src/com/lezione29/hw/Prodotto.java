package com.lezione29.hw;

public class Prodotto {
	
	private int prodottoID;
	private String nomeProdotto;
	private String codice;
	private float prezzo;
	private int quantitaDisponibile;
	
	Prodotto() {
		
	}
	
	public int getProdottoID() {
		return prodottoID;
	}
	public void setProdottoID(int prodottoID) {
		this.prodottoID = prodottoID;
	}
	public String getNomeProdotto() {
		return nomeProdotto;
	}
	public void setNomeProdotto(String nomeProdotto) {
		this.nomeProdotto = nomeProdotto;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	public int getQuantitaDisponibile() {
		return quantitaDisponibile;
	}
	public void setQuantitaDisponibile(int quantitaDisponibile) {
		this.quantitaDisponibile = quantitaDisponibile;
	}
	
	public String stampaProdotto() {
		return "Prodotto [prodottoID=" + prodottoID + ", nomeProdotto=" + nomeProdotto + ", codice=" + codice
				+ ", prezzo=" + prezzo + ", quantitaDisponibile=" + quantitaDisponibile + "]";
	}
	
}
