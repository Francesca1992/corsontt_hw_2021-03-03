package com.lezione29.hw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestioneProdotti {
	
	private ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();

	// SELECT
	public ArrayList<Prodotto> selectProdotti() throws SQLException {

		elencoProdotti.clear();
			
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String querySelect = "SELECT prodottoID, nomeProdotto, codice, prezzo, quantitaDisponibile FROM Prodotto";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);
		ResultSet risultato = ps.executeQuery();
		
		while (risultato.next()) {
			Prodotto prodTemp = new Prodotto();
			prodTemp.setProdottoID(risultato.getInt(1));
			prodTemp.setNomeProdotto(risultato.getString(2));
			prodTemp.setCodice(risultato.getString(3));
			prodTemp.setPrezzo(risultato.getFloat(4));
			prodTemp.setQuantitaDisponibile(risultato.getInt(5));

			this.elencoProdotti.add(prodTemp);
		}
			
		return elencoProdotti;
		
	}
	
	//INSERT
	public void insertProdotti(String varNomeProdotto, String varCodice, float varPrezzo, int varQuantitaDisponibile) {
		
		try {
			
			Connection conn = ConnettoreDB.getIstanza().getConnessione();
			
			String queryInsert = "INSERT INTO Prodotto (nomeProdotto, codice, prezzo, quantitaDisponibile) VALUES (?, ?, ?, ?)";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryInsert);
			ps.setString(1, varNomeProdotto);
			ps.setString(2, varCodice);
			ps.setFloat(3, varPrezzo);
			ps.setInt(4, varQuantitaDisponibile);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}