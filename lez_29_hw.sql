DROP DATABASE IF EXISTS lez_29_hw;
CREATE DATABASE lez_29_hw;
USE lez_29_hw;

CREATE TABLE Prodotto (
	prodottoID INTEGER NOT NULL AUTO_INCREMENT,
    nomeProdotto VARCHAR(150) NOT NULL,
    codice VARCHAR(20) NOT NULL UNIQUE,
    prezzo FLOAT NOT NULL,
    quantitaDisponibile INTEGER NOT NULL,
    PRIMARY KEY (prodottoID)
);

CREATE TABLE Utente (
	utenteID INTEGER NOT NULL AUTO_INCREMENT,
    nomeUtente VARCHAR(150) NOT NULL UNIQUE,
    passwordUtente VARCHAR(150) NOT NULL UNIQUE,
    PRIMARY KEY (utenteID)
);

INSERT INTO Prodotto (nomeProdotto, codice, prezzo, quantitaDisponibile) VALUES
("Prodotto1", "PROD1", 10.0, 4),
("Prodotto2", "PROD2", 1.5, 3),
("Prodotto3", "PROD3", 12.0, 0),
("Prodotto4", "PROD4", 10.45, 10),
("Prodotto5", "PROD5", 30, 6);

INSERT INTO Utente (nomeUtente, passwordUtente) VALUES
("francesca", "A123"),
("mario", "A124"),
("giacomo", "A125");